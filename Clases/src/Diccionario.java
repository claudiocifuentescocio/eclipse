public class Diccionario {

	/*
	 * Atributos
	 * */
	public String[][] capitales = {
			
			{"Chile","Santiago"},
			{"Venezuela","Caracas"},
			{"Per�","Lima"},
	};
	
	public String[][]ciudades =  {
	        {"Buenos Aires", "Capital de Argentina"},
			{"Santiago", "Capital de Chile"},
			{"Lima", "Capital de Per�"},
			{"Valdivia", "Capital Regional"},
			{"Puerto Montt", "Ciudad del Sur de Chile"},
			{"Valparaiso", "Ciudad Porte�a"},
			{"Concepci�n", "Ciudad del Rock"},
			{"Futrono", "Ciudad cordillerana"},
			{"Castro", "Capital de la Isla de Chilo�"}
			
			};
	
	private int anioPublicacion = 1996;
	
	/*
	 * Metodos
	 * */
	
	String retornoFecha() {
		return "15 de Mayo de "+anioPublicacion ;
	}
	
	public String buscarCiudades(String pais,String capital) {
		
		for(int i = 0; i<capitales.length;i++) {
			if(capitales[i][0].equals(pais)){
				
				return capitales[i][1];
				
			}
			
		}
		return null ;
	}
	
	
	public String obtenerDescripcion(String ciudad) {
		for(int i = 0;i<ciudades.length;i++) {
	    	
			if (ciudades[i][0].equals(ciudad)) {
				return ciudades[i][1];
			};		
		}
		return "la ciudad ingredada no se encuentra";
	
	}
}
